#! /usr/bin/python

import os
import sys
from optparse import OptionParser

def macro_fix(option, opt_str, value, parser):
  """Verify that all macros have the -D string so the
  macros can be passed directly to the pre-processor.
  """
  ret_array = []
  if getattr(parser.values, option.dest): 
    ret_array = getattr(parser.values, option.dest)
  for m in value.split():
    if m[:2] == '-D':
      ret_array.append(m)
    else:
      ret_array.append('-D'+m)
  setattr(parser.values, option.dest, ret_array)

def include_fix(option, opt_str, value, parser):
  """Verify that all include directories have the -I string
  so the include directories can be passed to the compiler.
  """
  ret_array = []
  if getattr(parser.values, option.dest):
    ret_array = getattr(parser.values, option.dest)
  for m in value.split():
    if m[:2] == '-I':
      ret_array.append(m)
    else:
      ret_array.append('-I'+m)
  setattr(parser.values, option.dest, ret_array)

def libpath_fix(option, opt_str, value, parser):
  """Verify that all library directories have the -L string
  so the library directories can be passed to the linker.
  """
  ret_array = []
  if getattr(parser.values, option.dest):
    ret_array = getattr(parser.values, option.dest)
  for m in value.split():
    if m[:2] == '-L':
      ret_array.append(m)
    else:
      ret_array.append('-L'+m)
  setattr(parser.values, option.dest, ret_array)

def lib_fix(option, opt_str, value, parser):
  """Verify that all libraries have the -l string
  so the libraries can be passed to the linker.
  """
  ret_array = []
  if getattr(parser.values, option.dest):
    ret_array = getattr(parser.values, option.dest)
  for m in value.split():
    if m[:2] == '-l':
      ret_array.append(m)
    else:
      ret_array.append('-l'+m)
  setattr(parser.values, option.dest, ret_array)

# Original mkmf command line options
# mkmf [-a abspath] [-c cppdefs] [-o otherflags] [-d] [-f] [-m makefile] 
#      [-p program] [-t template] [-v] [-x] [args]
usage = "Usage: %prog [options] args"
version = "%prog 1.0"
args_description = "args are a list of directories and files to be searched \
for targets and dependencies."
optParser = OptionParser(usage=usage, version=version, epilog=args_description)
optParser.add_option("-a", "--abspath", dest="abspath", default='.', 
                  type="string",
	          help="Attaches DIR at the front of all relative paths to "
                       "source files.  If \"-a\" is not specified, the "
                       "current working directory is the abspath.", 
                  metavar="DIR")
optParser.add_option("-v", "--verbose",
                  action="count", dest="verbose",
                  help="Verbose messaging.  Can be repeated to increase "
                       "verbosity") 
optParser.add_option("-m", "--makefile", dest="makefile", default="Makefile",
                  type="string",
                  help="The name of the makefile written (default Makefile).",
                  metavar="MAKEFILE")
optParser.add_option("-p", "--program", dest="program", default="a.out",
                  type="string",
                  help="The name of the final target (default a.out). Note: "
                       "if program has the file extension \".a\", it is "
                       "understood to be a library. The command to create "
                       "it is $(AR) $(ARFLAGS) instead of $(LD) $(LDFLAGS).",
                  metavar="PROG")
optParser.add_option("-t", "--template", dest="template", 
                  type="string",
                  help="file containing a list of make macros or commands "
                       "written to the beginning of the makefile",
                  metavar="TEMPLATE")
optParser.add_option("-c",
 	          action="append", dest="defines", 
                  type="string",
                  help="cpp #defines to be passed to the source files: "
                       "affected object files will be selectively removed if "
                       "there has been a change in this state.  Deprecated, "
                       "please use -D.",
                  metavar="cppdefs")
optParser.add_option("-D", 
 	          action="callback", dest="defines", 
                  type="string", callback=macro_fix,
                  help="cpp #defines to be passed to the source files: "
                       "affected object files will be selectively removed "
                       "if there has been a change in this state.",
                  metavar="MACRO")
optParser.add_option("-I",
                  action="callback", dest="includes", 
                  type="string", callback=include_fix,
                  help="Include directories added to the compile command as "
                       "\"-IDIR\".  These directories are not searched for "
                       "source/include files.",
                  metavar="DIR")
optParser.add_option("-L",
                  action="callback", dest="libPaths", 
                  type="string", callback=libpath_fix,
                  help="Additional library path locations added to the link "
                       "command as \"-LDIR\".",
                  metavar="DIR")
optParser.add_option("-l",
                  action="callback", dest="libs", 
                  type="string", callback=lib_fix,
                  help="Additional libraries added to the link command as \"-lLIB\".",
                  metavar="LIB")
optParser.add_option("--other_fflags", dest="fflags", 
                  type="string",
                  help="Additional Fortran compiler options to use.  Added to "
                       "the end of the compile command.",
                  metavar="FFLAGS")
optParser.add_option("--other_cflags", dest="cflags", 
                  type="string",
                  help="Additional C compiler options to use.  Added to the "
                       "end of the compile command.",
                  metavar="CFLAGS")
optParser.add_option("--other_cxxflags", dest="cxxflags", 
                  type="string",
                  help="Additional CXX compiler options to use.  Added to the "
                       "end of the compile command.",
                  metavar="CXXFLAGS")
optParser.add_option("--other_ldflags", dest="ldflags", 
                  type="string",
                  help="Additional options to use on the link command.  Added "
                       "to the end of the link command.",
                  metavar="LDFLAGS")

(options, args) = optParser.parse_args()

# Print out information for debugging
if options.verbose > 0:
  if options.defines:
    print "Macros: " + " ".join(options.defines)
  if options.includes:
    print "Include dirs: " + " ".join(options.includes)
  if options.libPaths:
    print "Library dirs: " + " ".join(options.libPaths)
  if options.libs:
    print "Libraries: " + " ".join(options.libs)
