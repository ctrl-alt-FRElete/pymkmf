"""A simple group of routines to search for files.
"""
import os
import fnmatch

__version__ = "2.0a1"
__FORTRAN_FILES = ( '*.f90', '*.F90',
                    '*.f',   '*.F',
                    '*.for', '*.FOR',
                    '*.ftn', '*.FTN',
                    '*.fpp', '*.FPP',
                    '*.f95', '*.F95',
                    '*.f03', '*.F03' )
__C_FILES = ( '*.C',   '*.c', 
                '*.cc',  '*.cp',
                '*.cpp', '*.cxx',
                '*.c++' )
__INC_FILES = ( '*.h',   '*.H',
                '*.inc', '*.INC', '*.fh' )
__DOC_FILES = ( '*.html', '*.txt',
                '*.pdf',  '*.ps',
                '*.jpg',  '*.png',  '*.gif', 
                'README', 'readme', 'read_me' )

def find_files(directory, exclude_dirs=None, pattern='*', followlinks=False):
  """Find all files recursively in directory, that match pattern,
  excluding all directories that match exclude_dirs.
  pattern and exclude_dirs can be either a simple string, or a list of strings."""
  found = []

  for root, dirs, files in os.walk(directory, topdown=True, followlinks=followlinks):
    if exclude_dirs is not None: dirs = [d for d in dirs if d not in exclude_dirs]

    for match in pattern:
      for f in fnmatch.filter(files,match):
        found.append(os.path.join(root,f))

  return found

def find_source_files(directory, exclude_dirs=None, followlinks=False):
  """Find all source files in directory"""
  global __FORTRAN_FILES, __C_FILES

  return find_files(directory, exclude_dirs=exclude_dirs, pattern=__FORTRAN_FILES+__C_FILES, followlinks=followlinks)

def find_all_source_files(directory, exclude_dirs=None, followlinks=False):
  """Find all source files in directory"""
  global __FORTRAN_FILES, __C_FILES, __INC_FILES

  return find_files(directory, exclude_dirs=exclude_dirs, pattern=__FORTRAN_FILES+__C_FILES+__INC_FILES, followlinks=followlinks)

def find_doc_files(directory, exclude_dirs, followlinks=False):
  """Find all file containing documentaion in directory"""
  global __DOC_FILES

  return find_files(directory, exclude_dirs=exclude_dirs, pattern=__DOC_FILES, followlinks=followlinks)
